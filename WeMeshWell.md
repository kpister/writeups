# WE MESH WELL #

A mesh project by Anfernee Goon and Kaiser Pister

The work we are submitting for a grade is on the master branch. Even though we may be pushing new content to other branches, those are simply for our own pleasure. Master will not be changed until after we receive a grade.
 
 
**WEMESHWELL IS WRITTEN ENTIRELY IN A UNIX ENVIRONMENT AND DOES NOT SUPPORT WINDOWS.**
 
**We provide an executable compiled from and for macOS as well as a make file which will work on most unix environments.**

**To make: make**

**To run: ./mesh file.off**
 
 
 
This project is designed to simplify and expand 3D triangle meshes.

The project is inspired by Professor Ravi Ramamoorthi for CSE 163.

### Mesh Viewer ###

We use standard OpenGL to view the OFF files. The behavior is defined in Geometry.cpp, shader.cpp, light.frag.glsl, light.vert.glsl, and display.cpp.

### Data Structure ###

The behavior of our data structure is defined in variables.h and used in decimation.cpp and readfile.cpp.

We use two lists to store our data. We have a face to vertex list and a vertex to face list.

The face to vertex list stores the three vertices that define a face (and the normal to that face). The vertex to face list stores the faces which neighbor the vertex.

The following is our very simple set of data structures:

```
#!c++
struct ftv {
    std::vector<int> vertices;
    int index;

    bool operator==(ftv f){
        //compare two faces. They are equal if the vertices are the same
    }

    int operator-(ftv f){
        return index - f.index;
    }

};
struct vtf {
    std::vector<ftv> faces;
    int index;
    vec3 vertex;

    bool operator==(vtf v){
        return (index == v.index);
    }

};

EXTERN std::vector<ftv> faceToVert;
EXTERN std::vector<vtf> vertToFace;
```

Here is a visualization of our data structure:
![IMG_20170519_222552.jpg](https://bitbucket.org/repo/KrrLBr7/images/4238237999-IMG_20170519_222552.jpg)

### Edge Collapse ###

The behavior of the edge collapse is defined in decimation.cpp.

Our edge collapse takes two vertices, and collapses them into a single vertex at the midpoint between the two vertices. We remove the two faces which neighbor on the edge and we also remove any fins which are created from the edge collapse.

We update the vertexToFace data structure to no longer include the removed faces. We adjust triIndices to no longer include the removed faces. By doing this, we do not need to adjust the size of triVerts2 or triNorms.

Here is an example of a single edge collapse removing fins:

Original | Collapse | Another Angle
-------- | -------- | -------------
![Screen Shot 2017-05-16 at 2.48.09 PM.png](https://bitbucket.org/repo/KrrLBr7/images/950979485-Screen%20Shot%202017-05-16%20at%202.48.09%20PM.png) | ![Screen Shot 2017-05-16 at 2.48.19 PM.png](https://bitbucket.org/repo/KrrLBr7/images/1744240705-Screen%20Shot%202017-05-16%20at%202.48.19%20PM.png) | ![Screen Shot 2017-05-16 at 2.48.53 PM.png](https://bitbucket.org/repo/KrrLBr7/images/1949677430-Screen%20Shot%202017-05-16%20at%202.48.53%20PM.png)

### Important Note ###

Our implementation is slower than we would like it to be. We were pressed for time and did not maintain a good level of efficiency. As a result, simplifying the cow by 500 edges can require up to 15 seconds, while more complicated meshes (like the bunny and brain) can take upwards of a minute or two (for 500 simplifications).

If we had more time this is the aspect of our code we would work on improving the most.


### Mesh Decimation ###
We implemented a simple (slow) version of the Garland and Heckbert Mesh Simplification algorithm.

You simplify the mesh by pressing 'd' while in the mesh viewer. This will perform n edge collapses, starting with the least error inducing, where n is the amount specified by the user. Amount begins at 5 and is adjusted with the '+, -' keys. Use the 'x' key to set amount to 500 and 'r' to reset back to 5.

Description of our process: we died.

Here are some images of a simple cow.![Screen Shot 2017-05-19 at 9.47.33 AM.png](https://bitbucket.org/repo/KrrLBr7/images/1163712081-Screen%20Shot%202017-05-19%20at%209.47.33%20AM.png)

![Screen Shot 2017-05-19 at 8.15.48 AM.png](https://bitbucket.org/repo/KrrLBr7/images/4056038527-Screen%20Shot%202017-05-19%20at%208.15.48%20AM.png)![Screen Shot 2017-05-19 at 8.44.43 AM.png](https://bitbucket.org/repo/KrrLBr7/images/2495633186-Screen%20Shot%202017-05-19%20at%208.44.43%20AM.png)
![Screen Shot 2017-05-19 at 8.44.54 AM.png](https://bitbucket.org/repo/KrrLBr7/images/3929381117-Screen%20Shot%202017-05-19%20at%208.44.54%20AM.png)![Screen Shot 2017-05-19 at 8.45.20 AM.png](https://bitbucket.org/repo/KrrLBr7/images/2199913192-Screen%20Shot%202017-05-19%20at%208.45.20%20AM.png)![Screen Shot 2017-05-19 at 8.45.31 AM.png](https://bitbucket.org/repo/KrrLBr7/images/2194800575-Screen%20Shot%202017-05-19%20at%208.45.31%20AM.png)![Screen Shot 2017-05-19 at 8.46.04 AM.png](https://bitbucket.org/repo/KrrLBr7/images/2140155379-Screen%20Shot%202017-05-19%20at%208.46.04%20AM.png)![Screen Shot 2017-05-19 at 8.46.18 AM.png](https://bitbucket.org/repo/KrrLBr7/images/3289949487-Screen%20Shot%202017-05-19%20at%208.46.18%20AM.png)![Screen Shot 2017-05-19 at 8.46.41 AM.png](https://bitbucket.org/repo/KrrLBr7/images/2839292323-Screen%20Shot%202017-05-19%20at%208.46.41%20AM.png)

### Progressive Mesh ###
We implemented a progressive mesh similar to the Hoppe 96 paper, however we were unable to get the geomesh to work properly. We slit vertices into two new vertices which have faces of their own. 

In order to use the progressive mesh you must first decimate the mesh to some extent (see above). Once a mesh is simplified, press the 'p' key to progress your mesh by 'amount' (the same amount as used in decimation).

Progression and decimation can be used together in (mostly) arbitrary order. (A mesh cannot be progressed when hasn't been simplified.)

The following is a video demonstrating our progressive mesh: 
[cow mesh](https://youtu.be/2wlMTGtkza0)