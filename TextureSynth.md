# TextMeUreSynthesis #

![second.jpg](https://bitbucket.org/repo/Bgke4zr/images/1270574485-second.jpg)

A Texture Synthesis, Texture Transfer and Image Analogies project developed by Kaiser Pister and Anfernee Goon as the final project for Professor Ravi Ramamoorthi's CSE 163 Advanced Graphics course at UCSD.

We followed the procedure outlined by Efros and Freeman in the paper *Image Quilting for Texture Synthesis and Texture Transfer*

We are in the process of adding Image Analogies to our project.

This program should work on any unix terminal (even windows?), however it was extensively tested on MacOs in terminal.

#How to install and make:#

Clone our repo; cd repo; make; done!

#How to use our program:#

./textsyn -type [-width w] [-height h] [-window w] [-map luminanceImage] inputfile [outputfile]

The following are valid types: -random, -mid, -min

Random will randomly select blocks with no restrictions.
 
Mid will select from a subset of blocks which provide some minimal error when connected to the image.

Min will use the same blocks as mid, but will now adjust how it adds the block to the new image. This algorithm is the same as outlined in the Efros and Freeman paper. We find the minimal error cut and follow along that edge.

Width and Height specify the output file's dimensions. Default = 2 * input texture

Window specifies the size of the block. A small window will have a hard time capturing pixels, a large window suffers from repetition. Default = 10 (we recommend values between 4 and 20)

Map is how we handle texture transfer. Specify the constraint image (the image which the texture will be applied to). Default = false

Input is a texture file. We support most file types. jpg, bmp, png all have been tested, however we use the FreeImage library which allows for many more.

Output is the name of the output file. Default = "synth+inputfile"


Here are some sample images of our favorite human:
Original image
![zuck.jpg](https://bitbucket.org/repo/Bgke4zr/images/1602398964-zuck.jpg)

input | output
----- | ------
![cheetah.jpg](https://bitbucket.org/repo/Bgke4zr/images/3856538288-cheetah.jpg) | ![first.jpg](https://bitbucket.org/repo/Bgke4zr/images/4192320654-first.jpg)
![chinese.jpg](https://bitbucket.org/repo/Bgke4zr/images/1044947115-chinese.jpg) | ![second.jpg](https://bitbucket.org/repo/Bgke4zr/images/1270574485-second.jpg)
![fire.jpg](https://bitbucket.org/repo/Bgke4zr/images/2961201375-fire.jpg) | ![third.jpg](https://bitbucket.org/repo/Bgke4zr/images/1045172156-third.jpg)
![thing.jpg](https://bitbucket.org/repo/Bgke4zr/images/2530342377-thing.jpg) | ![fourth.jpg](https://bitbucket.org/repo/Bgke4zr/images/3585721526-fourth.jpg)

As you can tell, some images work much better as input than others. Our favorite for quality is the Chinese character representation of the Zuck, however our favorite flavor is LitBerg (the fire one).

Playing with the window size can change a lot:

Here is a case study: original
![kaiser.jpg](https://bitbucket.org/repo/Bgke4zr/images/1870173644-kaiser.jpg)

With the cheetah.jpg (from above) window size 25

![seventh.jpg](https://bitbucket.org/repo/Bgke4zr/images/3056045584-seventh.jpg)

With the cheetah.jpg (from above) window size 8

![eighth.jpg](https://bitbucket.org/repo/Bgke4zr/images/2688428618-eighth.jpg)

With the chinese.jpg (from above) window size 8

![nineth copy.jpg](https://bitbucket.org/repo/Bgke4zr/images/1656617801-nineth%20copy.jpg)

#Plain-old Texture Synthesis#

Of course we can do this part too, but it isn't as exciting.

Here is a table of work.


Input: 

![chinese.jpg](https://bitbucket.org/repo/Bgke4zr/images/2763793212-chinese.jpg)

window 3 | window 7 | window 11 
-------- | -------- | --------- 
![c3.jpg](https://bitbucket.org/repo/Bgke4zr/images/2003323907-c3.jpg) | ![c7.jpg](https://bitbucket.org/repo/Bgke4zr/images/2757100654-c7.jpg) | ![c11.jpg](https://bitbucket.org/repo/Bgke4zr/images/1791602075-c11.jpg) 
**window 15** | **window 19** | **window 23**
![c15.jpg](https://bitbucket.org/repo/Bgke4zr/images/1006695284-c15.jpg) | ![c19.jpg](https://bitbucket.org/repo/Bgke4zr/images/4129450369-c19.jpg) | ![c23.jpg](https://bitbucket.org/repo/Bgke4zr/images/384704873-c23.jpg)

Here is an example of the 3 styles of Quilting (window size 15)

Original:

![thing.jpg](https://bitbucket.org/repo/Bgke4zr/images/3879677987-thing.jpg)


Random | Overlapping (not cut) | Overlapping (minimum cut)
------ | --------------------- | -------------------------
![tr.jpg](https://bitbucket.org/repo/Bgke4zr/images/3647880153-tr.jpg) | ![tmid.jpg](https://bitbucket.org/repo/Bgke4zr/images/4266064682-tmid.jpg) | ![tmin.jpg](https://bitbucket.org/repo/Bgke4zr/images/1550358720-tmin.jpg)