# README #
*IMPORTANT NOTE:* All the photos in this document are .png files. They were converted from .bmp files by MacOS

 
This is a simple photo editing suite which provides cropping, lighting adjustments and other features.

To use, simply make from terminal and run ./image with the required inputs

### Authors ###

* Created by Anfernee Goon and Kaiser Pister 
* Inspired by Professor Ravi Ramamoorthi CSE 163, Spring 2017
* Skeleton code provided by the TA's of the class

# Basic Operations #
### Change Brightness ###
To run:

```
#!bash

./image -brightness i < input.bmp > output.bmp
```

Please use values of i >= 0.
![changingbrightness.png](https://bitbucket.org/repo/ypKrMx7/images/1141049822-changingbrightness.png)

### Change Contrast ###
To run:

```
#!bash
./image -contrast i < input.bmp > output.bmp
```
![changingcontrast.png](https://bitbucket.org/repo/ypKrMx7/images/2010045008-changingcontrast.png)

### Change Saturation ###
To run:
```
#!bash
./image -saturation i < input.bmp > output.bmp
```
![changingsaturation.png](https://bitbucket.org/repo/ypKrMx7/images/747329729-changingsaturation.png)

### Gamma Correction ###
To run:
```
#!bash
./image -gamma i < input.bmp > output.bmp
```
Please use values of i > 0

### Crop ###
To run:
```
#!bash
./image -crop x y w h < input.bmp > output.bmp
```

Please use values of x < Width, y < Height, w > 0, w+x < Width, h > 0, h+y < Height

For input: ```./image crop 0 10 400 120 < wave.bmp > croppedwave.bmp```

![wave.png](https://bitbucket.org/repo/ypKrMx7/images/870520751-wave.png)

![croppedwave.png](https://bitbucket.org/repo/ypKrMx7/images/3323470678-croppedwave.png)


# Quantization and Dithering #

### Quantization ###
To run:
```
#!bash
./image -quantize i < input.bmp > output.bmp
```

Please use 0 < i < 8

Quantized |    
--------- | ------
![output11.png](https://bitbucket.org/repo/ypKrMx7/images/674380507-output11.png) | ![mandrill.png](https://bitbucket.org/repo/ypKrMx7/images/1324320673-mandrill.png)
i = 1 | original
![output12.png](https://bitbucket.org/repo/ypKrMx7/images/651629337-output12.png) | ![output13.png](https://bitbucket.org/repo/ypKrMx7/images/906553014-output13.png)
i = 2 | i = 3

### Random Dithering ###
To run:
```
#!bash
./image -randomDither i < input.bmp > output.bmp
```

Please use 0 < i < 8

Random Dither |     
------------- | -----------
![output21.png](https://bitbucket.org/repo/ypKrMx7/images/1093935928-output21.png) | ![mandrill.png](https://bitbucket.org/repo/ypKrMx7/images/1324320673-mandrill.png)
i = 1 | orignal
![output22.png](https://bitbucket.org/repo/ypKrMx7/images/3956379992-output22.png) | ![output23.png](https://bitbucket.org/repo/ypKrMx7/images/2928650675-output23.png)
i = 2 | i = 3

### Floyd Steinberg Dithering ###
To run:
```
#!bash
./image -fsd i < input.bmp > output.bmp
```

Please use 0 < i < 8

Floyd Steinberg |    
------------ | -------------
![output31.png](https://bitbucket.org/repo/ypKrMx7/images/3420430377-output31.png) | ![mandrill.png](https://bitbucket.org/repo/ypKrMx7/images/1324320673-mandrill.png)
i = 1 | original
![output32.png](https://bitbucket.org/repo/ypKrMx7/images/3710778876-output32.png) | ![output34.png](https://bitbucket.org/repo/ypKrMx7/images/2250217402-output34.png)
i = 2 | i = 3

Bits          | Quantize     | Random Dither | Floyd Steinman | Original
------------- | -------------| ------------- | -------------- | --------
1  | ![output11.png](https://bitbucket.org/repo/ypKrMx7/images/674380507-output11.png) | ![output21.png](https://bitbucket.org/repo/ypKrMx7/images/1093935928-output21.png)      | ![output31.png](https://bitbucket.org/repo/ypKrMx7/images/3420430377-output31.png) | ![mandrill.png](https://bitbucket.org/repo/ypKrMx7/images/1324320673-mandrill.png)
2  | ![output12.png](https://bitbucket.org/repo/ypKrMx7/images/651629337-output12.png) | ![output22.png](https://bitbucket.org/repo/ypKrMx7/images/3956379992-output22.png) | ![output32.png](https://bitbucket.org/repo/ypKrMx7/images/3710778876-output32.png) | ![mandrill.png](https://bitbucket.org/repo/ypKrMx7/images/1324320673-mandrill.png)
3  | ![output13.png](https://bitbucket.org/repo/ypKrMx7/images/906553014-output13.png) | ![output23.png](https://bitbucket.org/repo/ypKrMx7/images/2928650675-output23.png) | ![output33.png](https://bitbucket.org/repo/ypKrMx7/images/3520612156-output33.png) | ![mandrill.png](https://bitbucket.org/repo/ypKrMx7/images/1324320673-mandrill.png)
4  | ![output14.png](https://bitbucket.org/repo/ypKrMx7/images/3762337344-output14.png) | ![output24.png](https://bitbucket.org/repo/ypKrMx7/images/3480376608-output24.png) | ![output34.png](https://bitbucket.org/repo/ypKrMx7/images/2250217402-output34.png) | ![mandrill.png](https://bitbucket.org/repo/ypKrMx7/images/1324320673-mandrill.png)
5  | ![output15.png](https://bitbucket.org/repo/ypKrMx7/images/2687762382-output15.png) | ![output25.png](https://bitbucket.org/repo/ypKrMx7/images/3587300801-output25.png) | ![output35.png](https://bitbucket.org/repo/ypKrMx7/images/2401718218-output35.png) | ![mandrill.png](https://bitbucket.org/repo/ypKrMx7/images/1324320673-mandrill.png)

# Basic Convolution and Edge Detection #

### Blur ###
To run:
```
#!bash
./image -blur i < input.bmp > output.bmp 
```

Please use a value of i >= 3.

Even values will be incremented to the next odd value.

We follow the write-up's suggestions for filter, and to handle edge cases we reflect back on the image (eg. if x = -3, set x = 3).

Blur |  |   |    | 
---- | ----  | ------ | -----  | -------
![flower.png](https://bitbucket.org/repo/ypKrMx7/images/2133430252-flower.png) | ![output3.png](https://bitbucket.org/repo/ypKrMx7/images/2241159334-output3.png) | ![output5.png](https://bitbucket.org/repo/ypKrMx7/images/4032693525-output5.png) | ![output7.png](https://bitbucket.org/repo/ypKrMx7/images/3241639679-output7.png) | ![output9.png](https://bitbucket.org/repo/ypKrMx7/images/2635630698-output9.png)
orignal | i = 3 | i = 5 | i = 7 | i = 9
![output11.png](https://bitbucket.org/repo/ypKrMx7/images/3912844854-output11.png) | ![output13.png](https://bitbucket.org/repo/ypKrMx7/images/2139938357-output13.png) | ![output15.png](https://bitbucket.org/repo/ypKrMx7/images/4193309431-output15.png) | ![output17.png](https://bitbucket.org/repo/ypKrMx7/images/2652336692-output17.png) | ![output19.png](https://bitbucket.org/repo/ypKrMx7/images/1454467797-output19.png)
i = 11 | i = 13 | i = 15 | i = 17 | i = 19 

### Sharpen ###
To run: 
```
#!bash
./image -sharpen < input.bmp > output.bmp
```

Blur 3 | Sharpen | Original
------ | ------- | --------
![output3.png](https://bitbucket.org/repo/ypKrMx7/images/2241159334-output3.png) | ![outputsharp.png](https://bitbucket.org/repo/ypKrMx7/images/3264202070-outputsharp.png) | ![flower.png](https://bitbucket.org/repo/ypKrMx7/images/821939888-flower.png)

### Edge Detection ###
To run:
```
#!bash
./image -edgeDetect i < input.bmp > output.bmp
```

Threshold | Wave | Checkerboard | Checkerboard2*
--------- | ---- | ------------ | -------------
10 | ![wave10.png](https://bitbucket.org/repo/ypKrMx7/images/3628932749-wave10.png) | ![checker10.png](https://bitbucket.org/repo/ypKrMx7/images/288297367-checker10.png) | ![cchecker210.png](https://bitbucket.org/repo/ypKrMx7/images/1026086775-cchecker210.png)
100 | ![wave100.png](https://bitbucket.org/repo/ypKrMx7/images/637588266-wave100.png) | ![checker100.png](https://bitbucket.org/repo/ypKrMx7/images/2001962313-checker100.png) | ![cchecker2100.png](https://bitbucket.org/repo/ypKrMx7/images/2080324320-cchecker2100.png)
150 | ![wave150.png](https://bitbucket.org/repo/ypKrMx7/images/3838194964-wave150.png) | ![checker150.png](https://bitbucket.org/repo/ypKrMx7/images/459944693-checker150.png) | ![cchecker2150.png](https://bitbucket.org/repo/ypKrMx7/images/3658490451-cchecker2150.png)
200 | ![wave200.png](https://bitbucket.org/repo/ypKrMx7/images/1654544821-wave200.png) | ![checker200.png](https://bitbucket.org/repo/ypKrMx7/images/1063340475-checker200.png) | ![cchecker2200.png](https://bitbucket.org/repo/ypKrMx7/images/2459074440-cchecker2200.png)

*Checkerboard2 images have also been cropped post edge detection because the original files were too big to be uploaded to bitbucket.

# Anti-Aliasing #

### Scale ###
To run: 
```
#!bash
./image -sampling i -size width height < input.bmp > output.bmp
```

let i vary between 0 (Point approximation), 1 (Hat filter, linear interpolation), 2 (Mitchell Filter, cubic)

We dealt with all edge cases as reflections.

Dimensions | Point | Hat | Mitchell
---------- | ----- | --- | --------
300x300 | ![output0300300.png](https://bitbucket.org/repo/ypKrMx7/images/3086243256-output0300300.png) | ![output1300300.png](https://bitbucket.org/repo/ypKrMx7/images/802501142-output1300300.png) | ![output2300300.png](https://bitbucket.org/repo/ypKrMx7/images/127545571-output2300300.png)
300x512 | ![output0300512.png](https://bitbucket.org/repo/ypKrMx7/images/2414755734-output0300512.png) | ![output1300512.png](https://bitbucket.org/repo/ypKrMx7/images/1777557909-output1300512.png) | ![output2300512.png](https://bitbucket.org/repo/ypKrMx7/images/336733529-output2300512.png)
512x300 | ![output0512300.png](https://bitbucket.org/repo/ypKrMx7/images/281025401-output0512300.png) | ![output1512300.png](https://bitbucket.org/repo/ypKrMx7/images/3688678884-output1512300.png) | ![output2512300.png](https://bitbucket.org/repo/ypKrMx7/images/599465350-output2512300.png)
800x800* | ![output0400400.png](https://bitbucket.org/repo/ypKrMx7/images/2100049282-output0400400.png) | ![output1400400.png](https://bitbucket.org/repo/ypKrMx7/images/259210359-output1400400.png) | ![output2400400.png](https://bitbucket.org/repo/ypKrMx7/images/849599244-output2400400.png)


*Due to the size of the 800x800 files, we cropped to 400x400 so that bitbucket would accept them. They are still representative of the original output. They start at 0,0.

Even though you can download any of the above images to see them better, we included 75x75 cropped versions so you can see detail better (if it helps).

Dimensions | Point | Hat | Mitchell
---------- | ----- | --- | --------
300x300 | ![coutput0300300.png](https://bitbucket.org/repo/ypKrMx7/images/3442573970-coutput0300300.png) | ![coutput1300300.png](https://bitbucket.org/repo/ypKrMx7/images/1267787420-coutput1300300.png) | ![coutput2300300.png](https://bitbucket.org/repo/ypKrMx7/images/3600527065-coutput2300300.png)
300x512 | ![coutput0300512.png](https://bitbucket.org/repo/ypKrMx7/images/1367947990-coutput0300512.png) | ![coutput1300512.png](https://bitbucket.org/repo/ypKrMx7/images/1876384935-coutput1300512.png) | ![coutput2300512.png](https://bitbucket.org/repo/ypKrMx7/images/510693854-coutput2300512.png)
512x300 | ![coutput0512300.png](https://bitbucket.org/repo/ypKrMx7/images/1796701347-coutput0512300.png) | ![coutput1512300.png](https://bitbucket.org/repo/ypKrMx7/images/1779337410-coutput1512300.png) | ![coutput2512300.png](https://bitbucket.org/repo/ypKrMx7/images/3173137768-coutput2512300.png)
800x800 | ![coutput0800800.png](https://bitbucket.org/repo/ypKrMx7/images/2828658753-coutput0800800.png) | ![coutput1800800.png](https://bitbucket.org/repo/ypKrMx7/images/4147155280-coutput1800800.png) | ![coutput2800800.png](https://bitbucket.org/repo/ypKrMx7/images/263320235-coutput2800800.png)


### Shift ###
To run:
```
#!bash
./image -sampling i -shift x y < input.bmp > output.bmp
```

let i vary between 0 (Point approximation), 1 (Hat filter, linear interpolation), 2 (Mitchell Filter, cubic)

x and y are the amounts to shift each axis (integer or floating point)

Image | Shift | Point | Hat | Mitchell
----- | ----- | ----- | --- | --------
shed.bmp | 10 10 | ![output01010.png](https://bitbucket.org/repo/ypKrMx7/images/2483485646-output01010.png)| ![output11010.png](https://bitbucket.org/repo/ypKrMx7/images/1959685514-output11010.png) | ![output21010.png](https://bitbucket.org/repo/ypKrMx7/images/3785690851-output21010.png)
shed.bmp | 10.5 10.5 | ![output0105105.png](https://bitbucket.org/repo/ypKrMx7/images/3677676101-output0105105.png) | ![output1105105.png](https://bitbucket.org/repo/ypKrMx7/images/3913335800-output1105105.png) | ![output2105105.png](https://bitbucket.org/repo/ypKrMx7/images/3190855276-output2105105.png)
shed.bmp | 133.7 133.7 | ![output0133133.png](https://bitbucket.org/repo/ypKrMx7/images/3191540490-output0133133.png) | ![output1133133.png](https://bitbucket.org/repo/ypKrMx7/images/4112704072-output1133133.png) | ![output2133133.png](https://bitbucket.org/repo/ypKrMx7/images/871073212-output2133133.png)


*The input file was a scaled down version of shed.bmp (256 x 192)


# FUN #
To run: 
```
#!bash
./image [-maxgravity i] -fun x y < input.bmp > output.bmp
```

You have the option to set the width (maxgravity) of the gravity dot through the first option. It defaults to 50. x and y are the coordinates of the gravity dot. They must be on the image proper. 

Description: We create a "gravity dot" in the image which "pulls" pixels around it. All the pixels in the range will be adjusted by the pixels farther away from the center, but still within the event horizon. The impact a pixel has is inversely proportional to the square of its distance to the center.

Here are some sample outputs.
![coutput.png](https://bitbucket.org/repo/ypKrMx7/images/2370828651-coutput.png)


![output1.png](https://bitbucket.org/repo/ypKrMx7/images/4148597375-output1.png)
![output2.png](https://bitbucket.org/repo/ypKrMx7/images/1222298906-output2.png)
![output3.png](https://bitbucket.org/repo/ypKrMx7/images/417632693-output3.png)
![output4.png](https://bitbucket.org/repo/ypKrMx7/images/347353181-output4.png)